<?php

namespace Drupal\current_user_profile_condition\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides the 'Current user's profile' condition plugin.
 *
 * @Condition(
 *   id = "сurrent_user_profile",
 *   label = @Translation("Current user's profile"),
 * )
 */
class CurrentUserProfile extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Path current.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $pathCurrent;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Constructs a CurrentUserProfile plugin.
   *
   * @param \Drupal\Core\Path\CurrentPathStack $path_current
   *   The path_current service.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current_user service.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(CurrentPathStack $path_current, AccountInterface $account, array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->pathCurrent = $path_current;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('path.current'),
      $container->get('current_user'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildConfigurationForm($form, $form_state);
    unset($form['negate']);

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Display block only on a current user's profile page."),
      '#default_value' => $this->configuration['enabled'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['enabled'] = $form_state->getValue('enabled');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['enabled' => 0] + parent::defaultConfiguration();
  }

  /**
   * Evaluates the condition and returns TRUE or FALSE accordingly.
   *
   * @return bool
   *   TRUE if the condition has been met, FALSE otherwise.
   */
  public function evaluate() {

    if ($this->configuration['enabled'] == 0) {
      return TRUE;
    }

    return ($this->pathCurrent->getPath() == '/user/' . $this->account->id());
  }

  /**
   * Provides a human readable summary of the condition's configuration.
   */
  public function summary() {
    return $this->t("Return true if a current page is a current user's profile.");
  }

}
